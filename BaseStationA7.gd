extends "res://Interactable.gd"

onready var _npc : Node2D = $NPC
onready var _checkpoint = $RespawnPoint
var stage = 0
var dialogue_index = 0
var stage_dialogues = [
	[
		"I haven't seen anyone around in a long time",
		"We are stuck in this endless hole",
		"In the next room there will be a very strong enemy",
		"Your weakness is the same as yours",
		""
	]
]

func interact(player):
	player.add_fuel(15)
	_npc.play_dialogue()
	yield(get_tree().create_timer(3), "timeout")
	player.show_message("Fuel refueled")
