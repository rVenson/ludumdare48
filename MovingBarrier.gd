extends Node2D

export var disabled = false

func _ready():
	if disabled:
		$AnimationPlayer.stop()
	else:
		$AnimationPlayer.play("working")

func enable():
	$AnimationPlayer.play("working")

func disable():
	$AnimationPlayer.stop()
