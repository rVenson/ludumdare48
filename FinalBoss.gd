extends KinematicBody2D

enum fire_strategy_type { ONE_DIRECTION, AIM_TARGET }

onready var _gun_position = $GunPosition
onready var _detect_shape = $DetectionArea/CollisionShape2D
onready var _animator = $AnimationPlayer
onready var _label = $Label
onready var _raycast = $RayCast2D
onready var _nav : Navigation2D = $Navigation2D
onready var _damage_animator = $DamagePlayer
onready var _collider = $CollisionShape2D
onready var _mesh = $Mesh
onready var _death_particles = $DeathParticles

export var projectile_resource : PackedScene
export var fire_delay = 2000
export var fire_strength = 300
export var detection_radius = 1500
export var always_fire = false
export var speed = 2
export(fire_strategy_type) var fire_strategy = fire_strategy_type.AIM_TARGET
export var barrier_path : NodePath
var target : Node2D = null
var next_fire_time = 0
var life = 2

func _ready():
	set_detection_radius(detection_radius)

func _physics_process(delta):
	var is_target_valid = target and target.is_inside_tree()
	if(is_target_valid or always_fire):
		var path = _nav.get_simple_path(global_position, target.global_position)
		var target_next_point = Vector2(0, 0)
		if path.size() > 1:
			target_next_point = path[1]
		look_at(target_next_point)
		var direction = transform.origin.direction_to(target_next_point)
		move_and_collide(direction * speed)
		if(OS.get_ticks_msec() > next_fire_time):
			fire()
			calc_next_fire()

func calc_next_fire():
	next_fire_time = OS.get_ticks_msec() + fire_delay

func fire():
	var projectile : RigidBody2D = projectile_resource.instance()
	projectile.global_transform.origin = _gun_position.global_transform.origin
	var target_direction = Vector2.UP
	match(fire_strategy):
		fire_strategy_type.AIM_TARGET:
			target_direction = _gun_position.global_position.direction_to(target.global_position)
			projectile.look_at(target.global_position)
		fire_strategy_type.ONE_DIRECTION:
			projectile.look_at(_gun_position.global_position + target_direction)
	projectile.add_force(Vector2(0, 0), target_direction * fire_strength)
	get_tree().root.add_child(projectile)

func is_target_on_raycast():
	if target:
		_raycast.cast_to = target.global_position
		var collider : Node = _raycast.get_collider()
		if collider and collider.is_in_group('player'):
			return true
		return false

func set_detection_radius(radius):
	_detect_shape.shape.radius = radius

func show_message(msg):
	_label.text = msg
	_animator.stop(true)
	_animator.play("target_acquired")

func _on_DetectionArea_body_entered(body : Node2D):
	if body.is_in_group("player") and !always_fire:
		target = body
		show_message("Target Acquired")
		calc_next_fire()

func _on_DetectionArea_body_exited(body):
	if body.is_in_group("player") and !always_fire:
		show_message("Target Lost")
		target = null

func damage():
	_damage_animator.play("damage")
	life -= 1
	if(life <= 0):
		kill()

func kill():
	_collider.disabled = true
	set_physics_process(false)
	set_process(false)
	_mesh.hide()
	_death_particles.emitting = true
	yield(get_tree().create_timer(1), "timeout")
	get_node(barrier_path).queue_free()
