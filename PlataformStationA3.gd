extends "res://Interactable.gd"

var is_used = false
export var key_plataform : NodePath

func interact(player):
	if !is_used:
		if player.has_cargo('key'):
			var plataform = get_node(key_plataform)
			plataform.enable()
			player.unload_cargo('key')
			player.show_message("Key Used!")
		else:
			player.show_message("You don't have any key")
