extends Control

func _ready():
	GameVariables.end_game_screen = self

func show():
	var timestamp = OS.get_ticks_msec() - GameVariables.start_time
	$VBoxContainer/Value.text = str(timestamp / 1000)
	visible = true

func _on_Button_pressed():
	get_tree().reload_current_scene()
