extends RigidBody2D

func _on_Projectile_body_entered(body : Node2D):
	if body.is_in_group('boss'):
		body.damage()
	self.queue_free()

func _ready():
	yield(get_tree().create_timer(5), "timeout")
	queue_free()
