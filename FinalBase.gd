extends "res://Interactable.gd"

onready var _npc : Node2D = $NPC
onready var _checkpoint = $RespawnPoint
var stage = 0
var dialogue_index = 0
var stage_dialogues = [
	[
		"Well it looks like someone finally came to get us",
		"We ran out of power in a previous LDJAM and it would be impossible to go back",
		"I'm sorry that the end of this game seems bad",
		"The developer is very happy that you came here, because the game works!",
		"You should blame him, not me",
		"But now that we have a ship and fuel we can go home",
		"We have fuel, right?",
		""
	]
]

func _ready():
	_npc.dialog = stage_dialogues[0]

func interact(player):
	player.disable_move()
	self.player = player
	_npc.play_dialogue()

func _on_NPC_end_dialogue():
	GameVariables.end()
