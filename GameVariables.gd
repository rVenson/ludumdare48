extends Node

var respawn_point
var start_time = 0
var end_game_screen
var hud
var scenario : Node2D
var player

func update_respawn_point(node):
	respawn_point = node.global_position

func end():
	if end_game_screen:
		end_game_screen.show()
	if hud:
		hud.hide()
	scenario.pause_mode = Node.PAUSE_MODE_STOP
