extends Node2D

signal end_dialogue
onready var _label = $Label
onready var _animator = $AnimationPlayer
var player = null

var dialog_index = 0
var dialog = []

func _input(event):
	if player and event.is_action_pressed("interact"):
		play_dialogue()

func play_dialogue():
	if(_animator.is_playing()):
		_animator.advance(1000)
	else:
		_label.hide()
		_label.text = dialog[dialog_index]
		_animator.play("talking")
		_label.show()
		dialog_index += 1
		if(dialog_index >= dialog.size()):
			emit_signal("end_dialogue")
			dialog_index = 0

func stop_dialogue():
	_animator.stop(true)
	_label.percent_visible = 0
	dialog_index = 0

func _on_DetectArea_body_entered(body : Node):
	if body.is_in_group("player"):
		player = body
		play_dialogue()

func _on_DetectArea_body_exited(body : Node):
	if body.is_in_group("player"):
		player = null
		
