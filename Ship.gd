extends RigidBody2D

signal player_ready
signal player_killed
onready var _engine_particles : CPUParticles2D = $EngineTrailParticle
onready var _collider : CollisionPolygon2D = $CollisionPolygon2D
onready var _mesh = $Mesh
onready var _death_particles : CPUParticles2D = $DeathParticles
onready var _label : Label = $Label
onready var _animator : AnimationPlayer = $AnimationPlayer

var rotation_force = 3000.0
var engine_force = 3000.0
var throttle_level = 0.0
var fuel = 5.0
var yaw_level = 0.0
var is_killed = false
var storage_mass = 0
var crew_mass = 100
var ship_mass = 400
var cargo = []
var max_fuel = 5.0
export var god_mode = false

func _ready():
	GameVariables.player = self
	emit_signal("player_ready", self)

func _unhandled_input(event : InputEvent):
	if event.is_action_pressed("interact"):
		var colliding_bodies : Array = get_colliding_bodies()
		if colliding_bodies.size() > 0 and colliding_bodies[0].is_in_group('interactable'):
			if colliding_bodies[0].has_method('interact'):
				colliding_bodies[0].interact(self)
	if event is InputEventKey and event.scancode == KEY_ESCAPE and !is_killed:
		kill_god()

func _process(delta):
	_process_input()
	_process_engine_trail()

func _physics_process(delta):
	calc_mass()
	if(fuel > 0.0 or god_mode):
		_process_engine_trail()
		apply_central_impulse(-global_transform.y * engine_force * throttle_level)
		apply_torque_impulse(yaw_level * rotation_force)
		fuel -= throttle_level * delta * engine_force * 0.000065

func calc_mass():
	mass = ship_mass + crew_mass + storage_mass + (fuel * 3)

func _process_input():
	throttle_level = Input.get_action_strength("throttle")
	yaw_level = Input.get_action_strength("rotation_right")
	yaw_level -= Input.get_action_strength("rotation_left")

func _process_engine_trail():
	if fuel > 0:
		_engine_particles.linear_accel = -100 + (throttle_level * 100)
	else:
		_engine_particles.linear_accel = -100

func kill_god():
	rollback_cargo()
	is_killed = true
	custom_integrator = true
	_collider.disabled = true
	set_physics_process(false)
	set_process(false)
	sleeping = true
	_mesh.hide()
	_engine_particles.hide()
	_death_particles.emitting = true
	emit_signal("player_killed")
	yield(get_tree().create_timer(2), "timeout")
	rotation = 0
	global_transform.origin = GameVariables.respawn_point
	_mesh.show()
	_mesh.show()
	_engine_particles.show()
	sleeping = false
	set_physics_process(true)
	set_process(true)
	_collider.disabled = false
	custom_integrator = false
	is_killed = false
	fuel = max_fuel

func kill(force = false):
	rollback_cargo()
	if god_mode: return false
	is_killed = true
	custom_integrator = true
	_collider.disabled = true
	set_physics_process(false)
	set_process(false)
	sleeping = true
	_mesh.hide()
	_engine_particles.hide()
	_death_particles.emitting = true
	emit_signal("player_killed")
	yield(get_tree().create_timer(2), "timeout")
	rotation = 0
	global_transform.origin = GameVariables.respawn_point
	_mesh.show()
	_mesh.show()
	_engine_particles.show()
	sleeping = false
	set_physics_process(true)
	set_process(true)
	_collider.disabled = false
	custom_integrator = false
	is_killed = false
	fuel = max_fuel

func rollback_cargo():
	for c in cargo:
		if c.has('plataform'):
			c.plataform.rollback()

func show_message(msg):
	_label.text = msg
	_animator.play("show_message")

func add_fuel(quantity):
	fuel = min(fuel + quantity, max_fuel)

func load_cargo(item):
	storage_mass += item.mass
	cargo.append(item)

func has_cargo(item_name):
	for item in cargo:
		if(item.name == item_name):
			return true
	return false

func unload_cargo(item_name):
	for item in cargo:
		if(item.name == item_name):
			storage_mass -= item.mass
			cargo.erase(item)
			return

func disable_move():
	set_physics_process(false)
	
func enable_move():
	set_physics_process(true)

func _on_DamageArea_body_entered(body : Node):
	if !body.is_in_group('player') and !is_killed:
		kill()
