extends Area2D

var is_active = true
export var final_boss : Resource
onready var boss_position = $Position2D
var boss : Node2D

func _on_FinalSpawner_body_entered(body : Node2D):
	if is_active:
		if body.is_in_group('player'):
			boss = final_boss.instance()
			boss.global_position = boss_position.global_position
			get_parent().add_child(boss)
			is_active = false

func _on_PlayerShip_player_killed():
	if boss:
		boss.queue_free()
		boss = null
		is_active = false
