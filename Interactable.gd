extends StaticBody2D

var player : RigidBody2D

func _ready():
	var npc = get_node_or_null("NPC")
	if npc:
		npc.connect('end_dialogue', self, '_on_NPC_end_dialogue')

func interact(player):
	pass

func _on_NPC_end_dialogue():
	GameVariables.player.enable_move()
