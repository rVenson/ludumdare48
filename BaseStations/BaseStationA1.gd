extends "res://Interactable.gd"

onready var _npc : Node2D = $NPC
onready var _no_fuel_sign = $no_fuel_sign
onready var _checkpoint = $RespawnPoint
var stage = 0
var dialogue_index = 0
var stage_dialogues = [
	[
		"We running out of fuel here",
		"Please, help me to bring some fuel to the base",
		"Probably you can't go anyway without more fuel",
		"You will need a lot of fuel if you go through the wrong path",
		""
	],
	[
		"Wow! This exactly what we need here!",
		"I hope that you haven't stole those parts",
		"There's a lot of bandits on this cave these days",
		"Just a minute... I will upgrade your parts",
		"Here... just more a tweak here and...",
		"[SOC] ... [ZAK] ... [THUD] ...",
		"Okay, here is your upgraded fuel system",
		"Now you will have a lot more fuel to spend in your jorney",
		"Good trip my friend!",
		""
	],
	[
		"Hello my friend!",
		"How's it going?",
		""
	]
]

var required_item = "fuel_parts"

func interact(player):
	match(stage):
		0: 
			GameVariables.update_respawn_point(_checkpoint)
			player.show_message("Checkpoint updated!")
			_npc.dialog = stage_dialogues[0]
			stage = 1
			player.add_fuel(15)
			yield(get_tree().create_timer(3), "timeout")
			player.show_message("Fuel refueled")
		1:
			if player.has_cargo(required_item):
				stage = 2
				_npc.dialog = stage_dialogues[1]
		2: 
			if _npc.dialog_index == 6:
				player.max_fuel = 10
				player.fuel = player.max_fuel
				player.show_message("Fuel Capacity Upgraded to 10!")
				player.unload_cargo("fuel_parts")
				_no_fuel_sign.hide()
			if _npc.dialog_index == 0:
				_npc.dialog = stage_dialogues[2]
				stage = 3
		3:
			player.add_fuel(15)
			player.show_message("Fuel refueled")
	_npc.play_dialogue()
