extends "res://Interactable.gd"

onready var _npc : Node2D = $NPC
onready var _checkpoint = $RespawnPoint
var stage = 0
var dialogue_index = 0
var stage_dialogues = [
	[
		"You must be tired",
		"But there are still some challenges ahead",
		"Does this look like a game to you?",
		""
	]
]

func interact(player):
	match(stage):
		0: 
			GameVariables.update_respawn_point(_checkpoint)
			player.show_message("Checkpoint updated!")
			_npc.dialog = stage_dialogues[0]
			stage = 1
	player.add_fuel(15)
	_npc.play_dialogue()
	yield(get_tree().create_timer(3), "timeout")
	player.show_message("Fuel refueled")
