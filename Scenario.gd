extends Node2D

export var respawn_position : NodePath

func _ready():
	GameVariables.scenario = self
	GameVariables.update_respawn_point(get_node(respawn_position))
