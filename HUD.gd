extends Control

var player : RigidBody2D = null

func _ready():
	GameVariables.start_time = OS.get_ticks_msec()
	GameVariables.hud = self

func _process(delta):
	if player:
		var timestamp = OS.get_ticks_msec() - GameVariables.start_time
		$LeftPanel/Time/Value.text = str(timestamp / 1000)
		$RightPanel/HorizontalSpeed/Value.text = "%3.2f km/h" % (abs(floor(player.linear_velocity.x)) / 10)
		$RightPanel/VerticalSpeed/Value.text = "%3.2f km/h" % (abs(floor(player.linear_velocity.y)) / 10)
		if player.global_position.y > 0:
			$RightPanel/Altitude/Value.text = "%3.2fkm down" % (player.global_position.y / 1000)
		else:
			$RightPanel/Altitude/Value.text = "%3.2f m up" % abs(player.global_position.y)
		$LeftPanel/Fuel/Value.text = "%3.2f gallons" % player.fuel
		$LeftPanel/Mass/Value.text = "%3.2f kg" % player.mass

func _on_PlayerShip_player_ready(player):
	self.player = player
