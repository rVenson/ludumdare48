extends "res://Interactable.gd"

var in_transport_cargo = []

func interact(player):
	if $Items.get_child_count() > 0:
		var item = $Items.get_child(0)
		$Items.remove_child(item)
		in_transport_cargo.append(item)
		player.load_cargo({
			"name": "crew",
			"mass": 40,
			"plataform": self
		})
		player.show_message("+ 1 Passagenger")

func rollback():
	for cargo in in_transport_cargo:
		$Items.add_child(cargo)
	in_transport_cargo.clear()
