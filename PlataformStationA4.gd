extends "res://Interactable.gd"

var in_transport_cargo = []

func interact(player):
	if $Items.get_child_count() > 0:
		var item = $Items.get_child(0)
		$Items.remove_child(item)
		in_transport_cargo.append(item)
		player.load_cargo({
			"name": "LD",
			"mass": 0,
			"plataform": self
		})
		player.show_message("+ Mysterious Item")

func rollback():
	pass
