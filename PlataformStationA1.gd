extends "res://Interactable.gd"

var in_transport_cargo = []

func interact(player):
	if $Items.get_child_count() > 0:
		var item = $Items.get_child(0)
		$Items.remove_child(item)
		in_transport_cargo.append(item)
		player.add_fuel(5)
		player.load_cargo({
			"name": "fuel_parts",
			"mass": 200,
			"plataform": self
		})
		player.show_message("+ 5 Fuel and Fuel Parts")

func rollback():
	for cargo in in_transport_cargo:
		$Items.add_child(cargo)
	in_transport_cargo.clear()
