extends Node2D

onready var collider_list = $Colliders
onready var line_list = $Lines

func _ready():
	for line in line_list.get_children():
		var collider = CollisionPolygon2D.new()
		collider_list.add_child(collider)
		collider.polygon = line.points
