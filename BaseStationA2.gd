extends "res://Interactable.gd"

onready var _npc : Node2D = $NPC
onready var _checkpoint = $RespawnPoint
var stage = 0
var dialogue_index = 0
var stage_dialogues = [
	[
		"You won't go far if you can't unlock the next barrier",
		"You will need a key",
		"But I guess I shouldn't have said that",
		""
	]
]

func interact(player):
	match(stage):
		0: 
			GameVariables.update_respawn_point(_checkpoint)
			player.show_message("Checkpoint updated!")
			_npc.dialog = stage_dialogues[0]
			stage = 1
			player.add_fuel(15)
			yield(get_tree().create_timer(3), "timeout")
			player.show_message("Fuel refueled")
		1:
			player.add_fuel(15)
			player.show_message("Fuel refueled")
	_npc.play_dialogue()
