extends "res://Interactable.gd"

onready var _npc : Node2D = $NPC
onready var _checkpoint = $RespawnPoint
var stage = 0
var dialogue_index = 0
var stage_dialogues = [
	[
		"We lost contact with some workers from this base",
		"There was a collapse and a body of water blocked some passages",
		"We need them to reopen the road ahead ",
		""
	],
	[
		"You need to bring everyone here",
		"Go ahead or you'll never go deeper",
		"Poetic, isn't it?",
		""
	],
	[
		"All workers are here",
		"Now can you do us a favor and open the passage?"
	]
]
var saved_crew = 0

func interact(player):
	match(stage):
		0: 
			GameVariables.update_respawn_point(_checkpoint)
			player.show_message("Checkpoint updated!")
			_npc.dialog = stage_dialogues[0]
			stage = 1
		1:
			while player.has_cargo('crew'):
				player.unload_cargo('crew')
				saved_crew += 1
			if saved_crew < 3:
				_npc.dialog = stage_dialogues[1]
			else:
				player.load_cargo({
					"name": 'key',
					"mass": 0
				})
				_npc.dialog = stage_dialogues[2]
				stage = 2
	player.add_fuel(15)
	_npc.play_dialogue()
	yield(get_tree().create_timer(3), "timeout")
	player.show_message("Fuel refueled")
